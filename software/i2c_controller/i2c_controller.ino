#include <Wire.h>
const uint8_t address = 0x38;

void setup() {
  Wire.begin();        // join i2c bus (address optional for master)
  Serial.begin(115200);  // start serial for output
}

void write_byte(uint8_t b, uint8_t index){
  Wire.beginTransmission(address);
  Wire.write(b);
  Wire.endTransmission();
  Serial.println(index);   
}


void loop() {
  for (uint8_t pos = 0; pos < 8; pos ++) {
    // switch the relays, one after another
    // we need to invert the command-word since the relay-logic is inverted
    write_byte(0xFF - (1<<pos), pos);
    delay(20000);
  }
}
